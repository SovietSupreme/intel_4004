#include <time.h>

#include "4004.h"
#include "clear.h"



int main(int argc, char ** argv) {
   init_registers();
   writeROMToFile();
   unsigned char buffer[ 16 ];
   readFileIntoMemAt(buffer,"4004_test.rom",0,16);

   while(cycles < 16) {
       step_delay();
       //ClearScreen();
       cycles++;
      // instruction[buffer[cycles]].function();
      union INST inst;
      union OPCODE oc;
      union OPCODE arg;

      inst.dec.i = buffer[pc.dec.counter];
    
     oc.bits.bit0 = inst.bits.bit4;
      oc.bits.bit1 = inst.bits.bit5;
      oc.bits.bit2 = inst.bits.bit6;
      oc.bits.bit3 = inst.bits.bit7;
      
      arg.bits.bit0 = inst.bits.bit0;
      arg.bits.bit1 = inst.bits.bit1;
      arg.bits.bit2 = inst.bits.bit2;
      arg.bits.bit3 = inst.bits.bit3;
       printf("opcode: (%X) %d arg: %d\n",buffer[pc.dec.counter],oc.dec.i,arg.dec.i);
       pc.dec.counter++;
       
   }
   printf("MEM CHECK COMPLETE.\n");
   return 0;
}

void step_delay() {
	
    struct timespec req, rem;
	
	req.tv_sec = 0;
	req.tv_nsec = 1351.3513;
	
	nanosleep(&req, &rem);
}

void init_registers() {

    acc.dec.a = 0;
    pc.dec.counter = 0;

    lvl1.dec.i = 0;
    lvl2.dec.i = 0;
    lvl3.dec.i = 0;

    t.flag = 0;
    c.flag = 0;

    r0.dec.i = 0;
    r1.dec.i = 0;
    r2.dec.i = 0;
    r3.dec.i = 0;
    r4.dec.i = 0;
    r5.dec.i = 0;
    r6.dec.i = 0;
    r7.dec.i = 0;
    r8.dec.i = 0;
    r9.dec.i = 0;
    rA.dec.i = 0;
    rB.dec.i = 0;
    rC.dec.i = 0;
    rD.dec.i = 0;
    rE.dec.i = 0;
    rF.dec.i = 0;

    instruction[0x00] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x01] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x02] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x03] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x04] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x05] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x06] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x07] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x08] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x09] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x0A] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x0B] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x0C] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x0D] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x0E] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x0F] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x10] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x11] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x12] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x13] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x14] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x15] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x16] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x17] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x18] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x19] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x1A] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x1B] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x1C] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x1D] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x1E] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x1F] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x20] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x21] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x22] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x23] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x24] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x25] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x26] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x27] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x28] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x29] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x2A] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x2B] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x2C] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x2D] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x2E] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x2F] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x30] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x31] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x32] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x33] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x34] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x35] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x36] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x37] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x38] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x39] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x3A] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x3B] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x3C] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x3D] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x3E] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x3F] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x40] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x41] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x42] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x43] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x44] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x45] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x46] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x47] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x48] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x49] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x4A] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x4B] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x4C] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x4D] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x4E] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x4F] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x50] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x51] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x52] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x53] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x54] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x55] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x56] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x57] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x58] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x59] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x5A] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x5B] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x5C] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x5D] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x5E] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x5F] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x60] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x61] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x62] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x63] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x64] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x65] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x66] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x67] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x68] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x69] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x6A] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x6B] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x6C] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x6D] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x6E] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x6F] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x70] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x71] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x72] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x73] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x74] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x75] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x76] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x77] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x78] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x79] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x7A] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x7B] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x7C] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x7D] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x7E] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x7F] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x80] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x81] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x82] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x83] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x84] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x85] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x86] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x87] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x88] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x89] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x8A] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x8B] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x8C] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x8D] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x8E] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x8F] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x90] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x91] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x92] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x93] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x94] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x95] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x96] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x97] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x98] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x99] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x9A] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x9B] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x9C] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x9D] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x9E] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0x9F] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xA0] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xA1] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xA2] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xA3] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xA4] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xA5] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xA6] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xA7] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xA8] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xA9] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xAA] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xAB] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xAC] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xAD] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xAE] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xAF] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xB0] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xB1] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xB2] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xB3] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xB4] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xB5] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xB6] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xB7] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xB8] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xB9] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xBA] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xBB] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xBC] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xBD] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xBE] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xBF] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xC0] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xC1] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xC2] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xC3] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xC4] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xC5] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xC6] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xC7] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xC8] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xC9] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xCA] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xCB] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xCC] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xCD] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xCE] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xCF] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xD0] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xD1] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xD2] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xD3] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xD4] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xD5] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xD6] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xD7] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xD8] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xD9] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xDA] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xDB] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xDC] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xDD] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xDE] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xDF] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xE0] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xE1] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xE2] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xE3] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xE4] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xE5] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xE6] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xE7] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xE8] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xE9] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xEA] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xEB] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xEC] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xED] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xEE] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xEF] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xF0] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xF1] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xF2] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xF3] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xF4] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xF5] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xF6] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xF7] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xF8] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xF9] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xFA] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xFB] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xFC] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xFD] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xFE] = (Instruction) {"NOP", inst_NOP, ONE};
    instruction[0xFF] = (Instruction) {"NOP", inst_NOP, ONE};
}

void inst_NOP() {
    step_delay();
}

void init_tables() {
    //instructions[0x00] = 
}

static void readFileIntoMemAt(unsigned char* buffer, char* filename, uint32_t offset, uint32_t size) {
	
	FILE *f= fopen(filename, "rb");
	if (f==NULL) {
		printf("error: Couldn't open %s\n", filename);
		exit(1);
	}
	fseek(f, 0L, SEEK_END);
	fseek(f, 0L, SEEK_SET);

	uint8_t* tmp = (buffer + offset); // point to offset location in buffer and fill
	fread(tmp, size, 1, f);
	fclose(f);
}

static void writeROMToFile() {
	
	FILE* file = fopen("4004_test.rom","wb");
	
	if(file == NULL) {
		printf("Error opening file\n");
		exit(1);
	}
    //union INST instr[35] = 

    uint8_t rom[16] = {0x20,0x00,0x22,0x00,0xDC,0xB2,0x21,0xE0,0xF2,0x71,0x06,0x60,0x72,0x06,0x20,0x00};

	fwrite(rom,16,1,file);

	fclose(file);
}