#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#else // !_WIN32
#include <unistd.h>
#include <term.h>
#endif

void ClearScreen();